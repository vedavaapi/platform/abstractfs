import os
from typing import Union
import argparse

import fitz
from vedavaapi.objectdb.helpers import ObjModelException, projection_helper

from .. import AbstractOOLD, LocalOOLD, BaseFs, ComputationResult, AbstractFileResolver


class PdfFs(BaseFs):

    fs_name = 'pdffs'
    supported_namespaces = ['_vedavaapi', '_abstract']

    arg_parser = argparse.ArgumentParser(prog='pdffs')
    arg_parser.add_argument('--page', type=int)
    arg_parser.add_argument('--image_index', type=int, required=False)
    arg_parser.add_argument('--zoom', type=float, default=1)
    arg_parser.add_argument('--colorspace', type=str, choices=['RGB', 'GRAY', 'CMYK'], default='RGB')
    arg_parser.add_argument('--format', type=str, choices=['png'], default='png')

    @staticmethod
    def get_pdf(source_file: Union[LocalOOLD, AbstractOOLD]) -> fitz.Document:
        if isinstance(source_file, LocalOOLD):
            return fitz.Document(filename=source_file.path, filetype='pdf')
        name = source_file.name or ''
        if not name.endswith('.pdf'):
            name += '.pdf'
        return fitz.Document(stream=source_file.content, filename=name)

    def compute_info(
            self, source_file: Union[LocalOOLD, AbstractOOLD],
            projection: Union[dict, None] = None, **kwargs):

        projection_helper.validate_projection(projection)
        pdf = self.get_pdf(source_file)
        info = {"name": os.path.basename(pdf.name), "page_count": pdf.pageCount}
        should_compute_page_infos = True
        if projection:
            inclusion = list(projection.values())[0]
            should_compute_page_infos = bool(inclusion) == 'pages' in projection
        if should_compute_page_infos:
            page_infos = []
            for page in pdf:  # type: fitz.Page
                pi = {
                    "w": page.rect.width,
                    "h": page.rect.height,
                    "images": page.getImageList()
                }
                page_infos.append(pi)
            info['pages'] = page_infos
        pdf.close()
        projection_helper.project_doc(info, projection)
        return info

    @classmethod
    def _parse_args(cls, arg_str: str):
        try:
            args, _ = cls.arg_parser.parse_known_args(cls.args_to_cmd_args(arg_str))
        except Exception as e:
            raise ObjModelException('invalid pdffs arg_str', 400, attachments=e)
        return args

    def compute(self, source_file: Union[LocalOOLD, AbstractOOLD], arg_str: str) -> ComputationResult:
        args = self._parse_args(arg_str)
        pdf = self.get_pdf(source_file)
        if pdf.pageCount <= args.page:
            pdf.close()
            raise ObjModelException('page not found', 404)
        if args.image_index is not None:
            images = pdf.getPageImageList(args.page)
            if not 0 <= args.image_index < len(images):
                pdf.close()
                raise ObjModelException('pdfns: page-image not found', 404)
            image = images[args.image_index]
            pixmap = fitz.Pixmap(pdf, image[0])  # TODO n<5 cond
        else:
            zoom_matrix = fitz.Matrix(args.zoom, args.zoom)
            page = pdf[args.page]  # type: fitz.Page
            pixmap = page.getPixmap(matrix=zoom_matrix, alpha=False)  # type: fitz.Pixmap
        content = pixmap.getPNGData()
        pdf.close()
        # noinspection PyUnusedLocal,PyTypeChecker
        pixmap = None  # to release resources
        return ComputationResult(content, 'image/png', f'page-{args.page}')


def extract_page_image(source_fp, page_no, zoom=1.0, image_index=None, target_fp=None):
    lo = LocalOOLD(source_fp)
    # noinspection PyTypeChecker
    fs = PdfFs(None)
    pr = fs.compute(
        lo, f'page={page_no}&zoom={zoom}' + (f'&image_index={image_index}' if image_index is not None else ''))
    if target_fp:
        with open(target_fp, 'wb') as targetf:
            targetf.write(pr.content)
    return fs.compute_info(lo), pr


AbstractFileResolver.register_fs_cls(PdfFs)
