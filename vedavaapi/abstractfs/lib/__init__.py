import json
import shutil
from collections import namedtuple
from functools import reduce
from typing import Union, List, Optional
import os
import urllib.parse

from vedavaapi.acls import AclSvc
from vedavaapi.common.helpers.models import AgentContext, VVContext
from vedavaapi.objectdb.helpers import ObjModelException
from vedavaapi.objectdb.mydb import MyDbCollection
from vedavaapi.acls.permissions_helper import PermissionResolver


class AbstractOOLD(object):
    """
    To be used as lingua-franca, container, lazy-value
    """
    def __init__(
            self, source: Union[str, dict], op_str: str,
            resolver: 'AbstractFileResolver',
            agent_context: Optional[AgentContext] = None,
            cache_dir_path: Optional[str] = None,
            mimetype=None, content=None, name=None):
        self.source = source
        self.op_str = op_str
        self.resolver = resolver
        self.agent_context = agent_context
        self.set_cache_dir_path(cache_dir_path)

        self._mimetype = mimetype
        self._content = content
        self._name = name

        self._resolved = self._content is not None
        self._resolve_exception = None

    def _compute(self, info_only=False):
        if self._resolved:
            if self._resolve_exception:
                raise self._resolve_exception
            return
        if self.cache_dir_path and info_only and self._mimetype:
            return
        try:
            result = None
            if self.cache_dir_path:
                # noinspection PyBroadException
                try:
                    result = self.resolver.compute_from_cache(
                        self.cache_dir_path, info_only=info_only)
                except Exception:
                    pass
            if not result:
                result = self.resolver.compute(
                    self.source, self.op_str, agent_context=self.agent_context)
        except Exception as e:
            self._resolve_exception = e
            raise e
        if not(self.cache_dir_path and info_only):
            self._resolved = True
            self._content = result.content
        self._mimetype = result.mimetype
        self._name = result.name

    @property
    def mimetype(self):
        self._compute(info_only=True)
        return self._mimetype

    @property
    def content(self):
        self._compute()
        return self._content

    @property
    def name(self):
        self._compute(info_only=True)
        return self._name

    # noinspection PyAttributeOutsideInit
    def set_cache_dir_path(self, cache_dir_path):
        self.cache_dir_path = cache_dir_path
        self.cached_file_path = os.path.join(cache_dir_path, 'file.cache') if cache_dir_path else None

    def save(self, fp):
        with open(fp, 'wb') as f:
            f.write(self.content)


class LocalOOLD(object):
    def __init__(self, path, name=None, mimetype=None):
        self.path = path
        self.name = name
        if not name:
            self.name = os.path.basename(path)
        self.mimetype = mimetype

    @classmethod
    def from_oold(cls, oold: dict, objstore_data_root: str):
        file_path = os.path.join(objstore_data_root, oold['identifier'])
        namespace_data = oold.get('namespaceData', {})
        name = namespace_data.get('name')
        mimetype = namespace_data.get('mimetype')
        return cls(file_path, name=name, mimetype=mimetype)


ComputationResult = namedtuple('ComputationResult', ('content', 'mimetype', 'name'))


def get_doc_and_id(colln, res: Union[str, dict]):
    if type(res) == dict:
        return res['_id'], res
    doc = colln.get(res)
    if not doc:
        raise ObjModelException('invalid resource id', 400)
    return doc['_id'], doc


class BaseFs(object):

    supported_namespaces = []
    fs_name = 'base'

    @classmethod
    def effective_namespace(cls, namespace):
        if not namespace:
            return None
        if namespace in ['_vedavaapi', '_web', '_abstract']:
            return namespace
        return '_web'

    @classmethod
    def check_namespace(cls, ns):
        namespace = cls.effective_namespace(ns)
        if not namespace:
            raise ObjModelException('invalid abstract file source', 400)
        if namespace not in cls.supported_namespaces:
            raise ObjModelException('invalid input namespace', 400)
        return namespace

    @staticmethod
    def args_to_cmd_args(arg_str: str):
        try:
            argsl = urllib.parse.parse_qsl(
                arg_str, keep_blank_values=False, strict_parsing=True)
        except ValueError as e:
            raise ObjModelException('invalid arg_str', 400)
        return reduce(
            lambda args, p: args.extend((f'--{p[0]}', p[1])) or args, argsl, [])

    def __init__(self, resolver: 'AbstractFileResolver'):
        self.resolver = resolver

    def get_source_file(
            self, source: Union[str, dict], agent_context: Optional[AgentContext] = None):
        source_id, source_doc = get_doc_and_id(
            self.resolver.objstore_colln, source)
        if agent_context:
            resolved_perms = PermissionResolver.get_resolved_permissions(
                self.resolver.objstore_colln, self.resolver.acl_svc,
                source_doc, [PermissionResolver.READ],
                agent_context.user_id, agent_context.team_ids
            )
            if not resolved_perms.get(PermissionResolver.READ):
                raise ObjModelException(
                    'access denied for source document of abstract oold', 403)

        namespace = self.check_namespace(source_doc.get('namespace'))
        if namespace == '_vedavaapi':
            source_file = LocalOOLD.from_oold(
                source_doc, self.resolver.objstore_data_root)
        elif namespace == '_abstract':
            source_file = self.resolver.get(source_doc)
            source_file = LocalOOLD(
                source_file.cached_file_path, name=source_file.name, mimetype=source_file.mimetype)  # cached version
            #  source_file = self.resolver.resolve(source_doc['identifier'])
        else:
            raise ObjModelException(
                f'{self.fs_name} fs doesn\'t yet support remote source')
        return source_file

    def info(
            self, source: Union[str, dict],
            agent_context: Optional[AgentContext] = None, **kwargs) -> dict:
        return self.compute_info(
            self.get_source_file(source, agent_context=agent_context), **kwargs)

    def compute_info(
            self, source_file: Union[LocalOOLD, AbstractOOLD], **kwargs) -> dict:
        pass

    def resolve(
            self, source: Union[str, dict], arg_str: str,
            agent_context: Optional[AgentContext] = None) -> ComputationResult:
        return self.compute(
            self.get_source_file(source, agent_context=agent_context), arg_str)

    def compute(self, source_file: Union[LocalOOLD, AbstractOOLD], arg_str: str) -> ComputationResult:
        pass


class AbstractFileResolver(object):
    _fs_cls_registry = {}

    def __init__(
            self,
            objstore_colln: MyDbCollection, objstore_data_root: str,
            cache_root: str, acls_svc: AclSvc
    ):
        self.objstore_colln = objstore_colln
        self.objstore_data_root = objstore_data_root
        self.cache_root = cache_root
        if cache_root and not os.path.exists(cache_root):
            os.makedirs(os.path.join(cache_root, '/'), exist_ok=True)
        self.acl_svc = acls_svc
        self._fs_registry = {}

    @classmethod
    def register_fs_cls(cls, fs_cls: type(BaseFs)):
        cls._fs_cls_registry[fs_cls.fs_name] = fs_cls

    @staticmethod
    def identifier_components(identifier: str) -> List:
        if '/' not in identifier:
            raise ObjModelException('invalid abstract file identifier', 400)
        return identifier.split('/', 1)

    @classmethod
    def op_str_components(cls, op_str: str) -> List:
        if not op_str or ':' not in op_str:
            raise ObjModelException('invalid abstract file identifier', 400)
        return op_str.split(':', 1)

    def get_fs(self, fs_name: str) -> BaseFs:
        fs_cls = self._fs_cls_registry.get(fs_name, None)
        if not fs_cls:
            raise ObjModelException(
                'unknown abstract file system', http_response_code=400
            )

        if fs_name not in self._fs_registry:
            self._fs_registry[fs_name] = fs_cls(self)

        return self._fs_registry[fs_name]

    def compute(self, source: Union[str, dict], op_str: str,
                agent_context: Optional[AgentContext] = None) -> ComputationResult:
        fs_name, arg_str = self.op_str_components(op_str)
        fs = self.get_fs(fs_name)
        return fs.resolve(source, arg_str, agent_context=agent_context)

    @classmethod
    def compute_from_cache(cls, cache_dir_path, info_only=False):
        # noinspection PyBroadException
        try:
            with open(os.path.join(cache_dir_path, 'info.json'), 'rb') as infof:
                info = json.loads(infof.read().decode('utf-8'))
            if not info_only:
                with open(os.path.join(cache_dir_path, 'file.cache'), 'rb') as f:
                    content = f.read()
            else:
                content = None
            return ComputationResult(content, info.get('mimetype'), info.get('name'))
        except Exception:
            shutil.rmtree(cache_dir_path, ignore_errors=True)
            return None

    def resolve_from(
            self, source: Union[str, dict], op_str: str,
            agent_context: Optional[AgentContext] = None, lazy=True) -> AbstractOOLD:
        af = AbstractOOLD(source, op_str, self, agent_context=agent_context)
        if not lazy:
            # noinspection PyProtectedMember
            af._compute()
        return af

    def resolve(
            self, identifier: str,
            agent_context: Optional[AgentContext] = None, lazy=True) -> AbstractOOLD:
        source_id, op_str = self.identifier_components(identifier)
        return self.resolve_from(
            source_id, op_str, agent_context=agent_context, lazy=lazy)

    def info(
            self, fs_name: str, source: Union[str, dict],
            agent_context: Optional[AgentContext] = None, **kwargs) -> dict:
        fs = self.get_fs(fs_name)
        return fs.info(source, agent_context=agent_context, **kwargs)

    def get(self, oold: Union[str, dict], agent_context: Optional[AgentContext] = None):
        oold_id, oold_doc = get_doc_and_id(self.objstore_colln, oold)
        af = self.resolve(oold_doc['identifier'], agent_context=agent_context, lazy=True)

        cache_dir_path = os.path.join(self.cache_root, oold_id) + '/'
        if not os.path.exists(cache_dir_path):
            # noinspection PyProtectedMember
            af._compute()
            os.makedirs(cache_dir_path, exist_ok=True)
            with open(os.path.join(cache_dir_path, 'file.cache'), 'wb') as f:
                f.write(af.content)
            info = {"name": af.name, "mimetype": af.mimetype}
            with open(os.path.join(cache_dir_path, 'info.json'), 'wb') as infof:
                infof.write(json.dumps(info).encode('utf-8'))

        af = self.resolve(oold_doc['identifier'], lazy=True)
        af.set_cache_dir_path(cache_dir_path)
        return af

    def get_descriptor(self):
        return {
            "cache_root": self.cache_root
        }

    @classmethod
    def from_descriptor(cls, vv_context: VVContext, descriptor: dict):
        return cls(
            vv_context.colln, vv_context.data_dir_path,
            descriptor['cache_root'], vv_context.acl_svc
        )

from . import pdffs
