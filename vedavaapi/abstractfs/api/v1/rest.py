from io import BytesIO

import flask
import flask_restx
from vedavaapi.common.helpers.api_helper import error_response, jsonify_argument, check_argument_type
from vedavaapi.objectdb.helpers import ObjModelException

from .. import get_resolver
from . import api


def get_abstract_file(identifier):
    resolver = get_resolver()
    try:
        af = resolver.resolve(identifier)
        return flask.send_file(
            BytesIO(af.content), mimetype=af.mimetype
        )  # TODO provide attachment support also.
    except ObjModelException as e:
        return error_response(message=e.message, code=e.http_response_code)


@api.route('/<path:identifier>')
class AbstractFile(flask_restx.Resource):

    def get(self, identifier):
        return get_abstract_file(identifier)

@api.route('/<source>/<fs_name>/<arg_str>')
class AbstractFileFriendly(flask_restx.Resource):

    def get(self, source, fs_name, arg_str):
        return get_abstract_file(f'{source}/{fs_name}:{arg_str}')


@api.route('/<source>/<fs_name>/info.json')
class Info(flask_restx.Resource):

    get_parser = api.parser()
    get_parser.add_argument('opts', location='args', type=str, default='{}')

    @api.expect(get_parser, validate=True)
    def get(self, fs_name, source):
        args = self.get_parser.parse_args()
        opts = jsonify_argument(args.get('opts', None), key='opts')
        check_argument_type(opts, (dict,), key='opts', allow_none=True)

        resolver = get_resolver()
        try:
            info = resolver.info(fs_name, source, **opts)
        except ObjModelException as e:
            return error_response(message=e.message, code=e.http_response_code)
        return info
