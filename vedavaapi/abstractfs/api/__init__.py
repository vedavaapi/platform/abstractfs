from vedavaapi.common.helpers.api_helper import get_current_org

from .. import VedavaapiAbstractfs


def myservice():
    return VedavaapiAbstractfs.instance


def get_resolver():
    current_org_name = get_current_org()
    return myservice().resolver(current_org_name)


from .v1 import api_blueprint_v1


blueprints_path_map = {
    api_blueprint_v1: "/v1"
}
