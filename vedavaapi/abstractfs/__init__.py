from vedavaapi.common import VedavaapiService, OrgHandler

from .lib import AbstractFileResolver


class AbstractFsOrgHandler(OrgHandler):

    def __init__(self, service, org_name):
        super(AbstractFsOrgHandler, self).__init__(service, org_name)
        self._resolver = None

    def _new_resolver(self):
        objstore_svc = self.service.registry.lookup('objstore')
        acl_service = self.service.registry.lookup('acls')
        return AbstractFileResolver(
            objstore_svc.colln(self.org_name),
            objstore_svc.data_dir_path(self.org_name),
            self.store.file_store_path('cache', '/'),
            acl_service.get_acl_svc(self.org_name) if acl_service else None
        )

    @property
    def resolver(self):
        if not self._resolver:
            self._resolver = self._new_resolver()
        return self._resolver


class VedavaapiAbstractfs(VedavaapiService):

    instance = None  # type: VedavaapiAbstractfs

    dependency_services = ['objstore']
    org_handler_class = AbstractFsOrgHandler

    title = 'AbstractFs'
    description = 'AbstractFs'

    def __init__(self, registry, name, conf):
        super(VedavaapiAbstractfs, self).__init__(registry, name, conf)

    def resolver(self, org_name) -> AbstractFileResolver:
        return self.get_org(org_name).resolver
